/**
 * 
 */
package com.springmvc.model;

import java.io.Serializable;
import java.sql.Date;

/**
 * @author rbawa
 *
 */
public class WeekMasterModel implements Serializable{

	private Integer weekMasterId;
	private Integer weekNo;
	private Date fromDate;
	private Date toDate;
	private String username;
	/**
	 * @return the weekMasterId
	 */
	public Integer getWeekMasterId() {
		return weekMasterId;
	}
	/**
	 * @param weekMasterId the weekMasterId to set
	 */
	public void setWeekMasterId(Integer weekMasterId) {
		this.weekMasterId = weekMasterId;
	}
	/**
	 * @return the weekNo
	 */
	public Integer getWeekNo() {
		return weekNo;
	}
	/**
	 * @param weekNo the weekNo to set
	 */
	public void setWeekNo(Integer weekNo) {
		this.weekNo = weekNo;
	}
	/**
	 * @return the fromDate
	 */
	public Date getFromDate() {
		return fromDate;
	}
	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	/**
	 * @return the toDate
	 */
	public Date getToDate() {
		return toDate;
	}
	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

}
