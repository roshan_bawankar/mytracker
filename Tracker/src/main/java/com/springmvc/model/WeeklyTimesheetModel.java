/**
 * 
 */
package com.springmvc.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.springmvc.entity.Phases;
import com.springmvc.entity.Priority;
import com.springmvc.entity.Status;
import com.springmvc.entity.UserProject;
import com.springmvc.entity.WeekMaster;

/**
 * @author rbawa
 *
 */

public class WeeklyTimesheetModel implements Serializable{
	
		private Integer weeklyTimesheetId;
		private Integer statusId;
		private Integer priorityId;//
		
		private Integer phasesId;
		private Integer userProjectId;
		private String userProjectName;
		private BigDecimal sunday;//
		private BigDecimal saturday;//
		private BigDecimal friday;//
		private BigDecimal thursday;//
		private BigDecimal wednesday;//
		private BigDecimal tuesday;//
		private BigDecimal monday;//
		private String remarks;//
		private Integer weekMasterId;
		private String statusName;//
		private String priorityName;
		private String phaseName;//done
		private Integer projectId;//done
		private String aaType;
		private String actType;
		private String network;
		private String activity;
		private String activityNo;
		private String description;
		private Date fromDate;
		private Date toDate;
		private Integer weekNo;//
		private BigDecimal actualHours;
		/**
		 * @return the weeklyTimesheetId
		 */
		public Integer getWeeklyTimesheetId() {
			return weeklyTimesheetId;
		}
		/**
		 * @param weeklyTimesheetId the weeklyTimesheetId to set
		 */
		public void setWeeklyTimesheetId(Integer weeklyTimesheetId) {
			this.weeklyTimesheetId = weeklyTimesheetId;
		}
		/**
		 * @return the statusId
		 */
		public Integer getStatusId() {
			return statusId;
		}
		/**
		 * @param statusId the statusId to set
		 */
		public void setStatusId(Integer statusId) {
			this.statusId = statusId;
		}
		/**
		 * @return the priorityId
		 */
		public Integer getPriorityId() {
			return priorityId;
		}
		/**
		 * @param priorityId the priorityId to set
		 */
		public void setPriorityId(Integer priorityId) {
			this.priorityId = priorityId;
		}
		/**
		 * @return the phasesId
		 */
		public Integer getPhasesId() {
			return phasesId;
		}
		/**
		 * @param phasesId the phasesId to set
		 */
		public void setPhasesId(Integer phasesId) {
			this.phasesId = phasesId;
		}
		/**
		 * @return the userProjectId
		 */
		public Integer getUserProjectId() {
			return userProjectId;
		}
		/**
		 * @param userProjectId the userProjectId to set
		 */
		public void setUserProjectId(Integer userProjectId) {
			this.userProjectId = userProjectId;
		}
		/**
		 * @return the sunday
		 */
		public BigDecimal getSunday() {
			return sunday;
		}
		/**
		 * @param sunday the sunday to set
		 */
		public void setSunday(BigDecimal sunday) {
			this.sunday = sunday;
		}
		/**
		 * @return the saturday
		 */
		public BigDecimal getSaturday() {
			return saturday;
		}
		/**
		 * @param saturday the saturday to set
		 */
		public void setSaturday(BigDecimal saturday) {
			this.saturday = saturday;
		}
		/**
		 * @return the friday
		 */
		public BigDecimal getFriday() {
			return friday;
		}
		/**
		 * @param friday the friday to set
		 */
		public void setFriday(BigDecimal friday) {
			this.friday = friday;
		}
		/**
		 * @return the thursday
		 */
		public BigDecimal getThursday() {
			return thursday;
		}
		/**
		 * @param thursday the thursday to set
		 */
		public void setThursday(BigDecimal thursday) {
			this.thursday = thursday;
		}
		/**
		 * @return the wednesday
		 */
		public BigDecimal getWednesday() {
			return wednesday;
		}
		/**
		 * @param wednesday the wednesday to set
		 */
		public void setWednesday(BigDecimal wednesday) {
			this.wednesday = wednesday;
		}
		/**
		 * @return the tuesday
		 */
		public BigDecimal getTuesday() {
			return tuesday;
		}
		/**
		 * @param tuesday the tuesday to set
		 */
		public void setTuesday(BigDecimal tuesday) {
			this.tuesday = tuesday;
		}
		/**
		 * @return the monday
		 */
		public BigDecimal getMonday() {
			return monday;
		}
		/**
		 * @param monday the monday to set
		 */
		public void setMonday(BigDecimal monday) {
			this.monday = monday;
		}
		/**
		 * @return the remarks
		 */
		public String getRemarks() {
			return remarks;
		}
		/**
		 * @param remarks the remarks to set
		 */
		public void setRemarks(String remarks) {
			this.remarks = remarks;
		}
		/**
		 * @return the weekMasterId
		 */
		public Integer getWeekMasterId() {
			return weekMasterId;
		}
		/**
		 * @param weekMasterId the weekMasterId to set
		 */
		public void setWeekMasterId(Integer weekMasterId) {
			this.weekMasterId = weekMasterId;
		}
		/**
		 * @return the statusName
		 */
		public String getStatusName() {
			return statusName;
		}
		public BigDecimal getActualHours() {
			return actualHours;
		}
		public void setActualHours(BigDecimal actualHours) {
			this.actualHours = actualHours;
		}
		/**
		 * @param statusName the statusName to set
		 */
		public void setStatusName(String statusName) {
			this.statusName = statusName;
		}
		/**
		 * @return the priorityName
		 */
		public String getPriorityName() {
			return priorityName;
		}
		/**
		 * @param priorityName the priorityName to set
		 */
		public void setPriorityName(String priorityName) {
			this.priorityName = priorityName;
		}
		/**
		 * @return the phaseName
		 */
		public String getPhaseName() {
			return phaseName;
		}
		/**
		 * @param phaseName the phaseName to set
		 */
		public void setPhaseName(String phaseName) {
			this.phaseName = phaseName;
		}
		/**
		 * @return the projectId
		 */
		public Integer getProjectId() {
			return projectId;
		}
		/**
		 * @param projectId the projectId to set
		 */
		public void setProjectId(Integer projectId) {
			this.projectId = projectId;
		}
		/**
		 * @return the aaType
		 */
		public String getAaType() {
			return aaType;
		}
		/**
		 * @param aaType the aaType to set
		 */
		public void setAaType(String aaType) {
			this.aaType = aaType;
		}
		/**
		 * @return the actType
		 */
		public String getActType() {
			return actType;
		}
		/**
		 * @param actType the actType to set
		 */
		public void setActType(String actType) {
			this.actType = actType;
		}
		/**
		 * @return the network
		 */
		public String getNetwork() {
			return network;
		}
		/**
		 * @param network the network to set
		 */
		public void setNetwork(String network) {
			this.network = network;
		}
		/**
		 * @return the activity
		 */
		public String getActivity() {
			return activity;
		}
		/**
		 * @param activity the activity to set
		 */
		public void setActivity(String activity) {
			this.activity = activity;
		}
		/**
		 * @return the activityNo
		 */
		public String getActivityNo() {
			return activityNo;
		}
		/**
		 * @param activityNo the activityNo to set
		 */
		public void setActivityNo(String activityNo) {
			this.activityNo = activityNo;
		}
		/**
		 * @return the description
		 */
		public String getDescription() {
			return description;
		}
		/**
		 * @param description the description to set
		 */
		public void setDescription(String description) {
			this.description = description;
		}
		/**
		 * @return the weekNo
		 */
		public Integer getWeekNo() {
			return weekNo;
		}
		/**
		 * @param weekNo the weekNo to set
		 */
		public void setWeekNo(Integer weekNo) {
			this.weekNo = weekNo;
		}
		public String getUserProjectName() {
			return userProjectName;
		}
		public void setUserProjectName(String userProjectName) {
			this.userProjectName = userProjectName;
		}
		public Date getFromDate() {
			return fromDate;
		}
		public void setFromDate(Date fromDate) {
			this.fromDate = fromDate;
		}
		public Date getToDate() {
			return toDate;
		}
		public void setToDate(Date toDate) {
			this.toDate = toDate;
		}
		
}
