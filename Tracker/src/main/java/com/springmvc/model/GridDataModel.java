/**
 * 
 */
package com.springmvc.model;

import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;

/**
 * @author rbawa
 *
 */
public class GridDataModel {
	private Integer phaseId;
	private String phaseName;
	private Integer priorityId;
	private String priorityName;
	private Integer projectId;
	private String projectName;
	private String roleCode;
	private String roleName;
	private Integer statusId;
	private String statusName;
	private String username;
	private String personnelNo;
	private String name;
	private String password;
	private Integer userProjectId;
	private String aaType;
	private String actType;
	private String network;
	private String activity;
	private String activityNo;
	private String description;
	private Integer weeklyTimesheetId;
	private Integer phasesId;
	private BigDecimal sunday;
	private BigDecimal saturday;
	private BigDecimal friday;
	private BigDecimal thursday;
	private BigDecimal wednesday;
	private BigDecimal tuesday;
	private BigDecimal monday;
	private String remarks;
	private Integer weekMasterId;
	private Integer weekNo;
	private Date fromDate;
	private Date toDate;
}
