/**
 * 
 */
package com.springmvc.model;

/**
 * @author rbawa
 *
 */
public class UserProjectNamesModel {

	private Integer userProjectId;
	public Integer getUserProjectId() {
		return userProjectId;
	}
	public void setUserProjectId(Integer userProjectId) {
		this.userProjectId = userProjectId;
	}
	public String getUserProjectName() {
		return userProjectName;
	}
	public void setUserProjectName(String userProjectName) {
		this.userProjectName = userProjectName;
	}
	private String userProjectName;

}
