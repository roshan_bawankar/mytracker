package com.springmvc.model;

import java.math.BigDecimal;

/**
 * @author rbawa
 *
 */
public class UserProjectDetailsModel {
	
	private Integer userProjectId;
	private String userProjectName;
	private String aaType;
	private String actType;
	private String activity;
	private String activityNo;
	private String description;
	private String network;
	private Integer projectId;
	private String username;
	private BigDecimal actualHours;
	private BigDecimal estimatedHours;
	
	public Integer getUserProjectId() {
		return userProjectId;
	}
	public void setUserProjectId(Integer userProjectId) {
		this.userProjectId = userProjectId;
	}
	public String getUserProjectName() {
		return userProjectName;
	}
	public void setUserProjectName(String userProjectName) {
		this.userProjectName = userProjectName;
	}
	public String getAaType() {
		return aaType;
	}
	public void setAaType(String aaType) {
		this.aaType = aaType;
	}
	public String getActType() {
		return actType;
	}
	public void setActType(String actType) {
		this.actType = actType;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	public String getActivityNo() {
		return activityNo;
	}
	public void setActivityNo(String activityNo) {
		this.activityNo = activityNo;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getNetwork() {
		return network;
	}
	public void setNetwork(String network) {
		this.network = network;
	}
	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public BigDecimal getActualHours() {
		return actualHours;
	}
	public void setActualHours(BigDecimal actualHours) {
		this.actualHours = actualHours;
	}
	public BigDecimal getEstimatedHours() {
		return estimatedHours;
	}
	public void setEstimatedHours(BigDecimal estimatedHours) {
		this.estimatedHours = estimatedHours;
	}

}
