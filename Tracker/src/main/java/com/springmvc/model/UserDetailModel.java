/**
 * 
 */
package com.springmvc.model;

import java.io.Serializable;

/**
 * @author rbawa
 *
 */

public class UserDetailModel implements Serializable{
	private String username;
	private String personnelNo;
	private String name;
	private String roleCode;
	private String password;
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the personnelNo
	 */
	public String getPersonnelNo() {
		return personnelNo;
	}
	/**
	 * @param personnelNo the personnelNo to set
	 */
	public void setPersonnelNo(String personnelNo) {
		this.personnelNo = personnelNo;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the roleCode
	 */
	public String getRoleCode() {
		return roleCode;
	}
	/**
	 * @param roleCode the roleCode to set
	 */
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
}
