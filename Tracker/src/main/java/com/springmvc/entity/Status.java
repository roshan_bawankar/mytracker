package com.springmvc.entity;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import org.hibernate.annotations.Parameter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.hibernate.annotations.GenericGenerator;
@EnableTransactionManagement
@Entity
@Table(name="status")
public class Status{

@Id
@GeneratedValue
@Column(name="status_id",nullable=false)
private Integer statusId;

@Column(name="status_name",nullable=true,length=20)
private String statusName;

@OneToMany(fetch=FetchType.LAZY,mappedBy="statusId")
private Set<WeeklyTimesheet> weeklyTimesheetSetByStatusId;


public Integer getStatusId(){
return this.statusId;
}

public void setStatusId(Integer statusId){
this.statusId = statusId;
}

public String getStatusName(){
return this.statusName;
}

public void setStatusName(String statusName){
this.statusName = statusName;
}

public Set<WeeklyTimesheet> getWeeklyTimesheetSetByStatusId(){
return this.weeklyTimesheetSetByStatusId;
}

public void setWeeklyTimesheetSetByStatusId(Set<WeeklyTimesheet> weeklyTimesheetSetByStatusId){
this.weeklyTimesheetSetByStatusId = weeklyTimesheetSetByStatusId;
}
}