package com.springmvc.entity;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import org.hibernate.annotations.Parameter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.hibernate.annotations.GenericGenerator;
@EnableTransactionManagement
@Entity
@Table(name="phases")
public class Phases{

@Id
@GeneratedValue
@Column(name="phase_id",nullable=false)
private Integer phaseId;

@Column(name="phase_name",nullable=true,length=20)
private String phaseName;


public Integer getPhaseId(){
return this.phaseId;
}

public void setPhaseId(Integer phaseId){
this.phaseId = phaseId;
}

public String getPhaseName(){
return this.phaseName;
}

public void setPhaseName(String phaseName){
this.phaseName = phaseName;
}
}