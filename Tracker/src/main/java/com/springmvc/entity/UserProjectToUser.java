package com.springmvc.entity;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.transaction.annotation.EnableTransactionManagement;
@EnableTransactionManagement
@Entity
@Table(name="user_project_to_user")
public class UserProjectToUser{

@Id
@GeneratedValue
@Column(name="user_project_to_user_id",nullable=false)
private Integer userProjectToUserId;

@Column(name="user_project_id",nullable=false)
private Integer userProjectId;

@Column(name="username",nullable=false)
private String username;

@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name ="username",insertable=false,updatable=false)
private User userDetailByUsername;

public UserProject getUserProjectByUserProjectId() {
	return userProjectByUserProjectId;
}

public void setUserProjectByUserProjectId(UserProject userProjectByUserProjectId) {
	this.userProjectByUserProjectId = userProjectByUserProjectId;
}

@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name ="user_project_id",insertable=false,updatable=false)
private UserProject userProjectByUserProjectId;

@OneToMany(fetch=FetchType.LAZY,mappedBy="userProjectId")
private Set<WeeklyTimesheet> weeklyTimesheetSetByUserProjectId;




public Integer getUserProjectId(){
return this.userProjectId;
}

public void setUserProjectId(Integer userProjectId){
this.userProjectId = userProjectId;
}

public String getUsername(){
return this.username;
}

public void setUsername(String username){
this.username = username;
}

public User getUserDetailByUsername(){
return this.userDetailByUsername;
}

public void setUserDetailByUsername(User userDetailByUsername){
this.userDetailByUsername = userDetailByUsername;
}

public Set<WeeklyTimesheet> getWeeklyTimesheetSetByUserProjectId(){
return this.weeklyTimesheetSetByUserProjectId;
}

public void setWeeklyTimesheetSetByUserProjectId(Set<WeeklyTimesheet> weeklyTimesheetSetByUserProjectId){
this.weeklyTimesheetSetByUserProjectId = weeklyTimesheetSetByUserProjectId;
}
}