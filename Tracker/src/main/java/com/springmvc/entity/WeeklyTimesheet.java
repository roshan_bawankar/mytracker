package com.springmvc.entity;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import org.hibernate.annotations.Parameter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.hibernate.annotations.GenericGenerator;

import java.math.BigDecimal;
@EnableTransactionManagement
@Entity
@Table(name="weekly_timesheet")
public class WeeklyTimesheet{

@Id
@GeneratedValue
@Column(name="weekly_timesheet_id",nullable=false)
private Integer weeklyTimesheetId;

@Column(name="status_id",nullable=true)
private Integer statusId;

@Column(name="priority_id",nullable=true)
private Integer priorityId;

@Column(name="phases_id",nullable=true)
private Integer phasesId;

@Column(name="user_project_id",nullable=true)
private Integer userProjectId;

@Column(name="sunday",nullable=true,precision=4,scale=2)
private BigDecimal sunday;

@Column(name="saturday",nullable=true,precision=4,scale=2)
private BigDecimal saturday;

@Column(name="friday",nullable=true,precision=4,scale=2)
private BigDecimal friday;

@Column(name="thursday",nullable=true,precision=4,scale=2)
private BigDecimal thursday;

@Column(name="wednesday",nullable=true,precision=4,scale=2)
private BigDecimal wednesday;

@Column(name="tuesday",nullable=true,precision=4,scale=2)
private BigDecimal tuesday;

@Column(name="monday",nullable=true,precision=4,scale=2)
private BigDecimal monday;

@Column(name="remarks",nullable=true,length=100)
private String remarks;

@Column(name="week_master_id",nullable=true)
private Integer weekMasterId;

@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name ="status_id",insertable=false,updatable=false)
private Status statusByStatusId;

@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name ="priority_id",insertable=false,updatable=false)
private Priority priorityByPriorityId;

@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name ="phases_id",insertable=false,updatable=false)
private Phases phasesByPhasesId;

@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name ="user_project_id",insertable=false,updatable=false)
private UserProject userProjectByUserProjectId;

@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name ="week_master_id",insertable=false,updatable=false)
private WeekMaster weekMasterByWeekMasterId;


public Integer getWeeklyTimesheetId(){
return this.weeklyTimesheetId;
}

public void setWeeklyTimesheetId(Integer weeklyTimesheetId){
this.weeklyTimesheetId = weeklyTimesheetId;
}

public Integer getStatusId(){
return this.statusId;
}

public void setStatusId(Integer statusId){
this.statusId = statusId;
}

public Integer getPriorityId(){
return this.priorityId;
}

public void setPriorityId(Integer priorityId){
this.priorityId = priorityId;
}

public Integer getPhasesId(){
return this.phasesId;
}

public void setPhasesId(Integer phasesId){
this.phasesId = phasesId;
}

public Integer getUserProjectId(){
return this.userProjectId;
}

public void setUserProjectId(Integer userProjectId){
this.userProjectId = userProjectId;
}

public BigDecimal getSunday(){
return this.sunday;
}

public void setSunday(BigDecimal sunday){
this.sunday = sunday;
}

public BigDecimal getSaturday(){
return this.saturday;
}

public void setSaturday(BigDecimal saturday){
this.saturday = saturday;
}

public BigDecimal getFriday(){
return this.friday;
}

public void setFriday(BigDecimal friday){
this.friday = friday;
}

public BigDecimal getThursday(){
return this.thursday;
}

public void setThursday(BigDecimal thursday){
this.thursday = thursday;
}

public BigDecimal getWednesday(){
return this.wednesday;
}

public void setWednesday(BigDecimal wednesday){
this.wednesday = wednesday;
}

public BigDecimal getTuesday(){
return this.tuesday;
}

public void setTuesday(BigDecimal tuesday){
this.tuesday = tuesday;
}

public BigDecimal getMonday(){
return this.monday;
}

public void setMonday(BigDecimal monday){
this.monday = monday;
}

public String getRemarks(){
return this.remarks;
}

public void setRemarks(String remarks){
this.remarks = remarks;
}

public Integer getWeekMasterId(){
return this.weekMasterId;
}

public void setWeekMasterId(Integer weekMasterId){
this.weekMasterId = weekMasterId;
}

public Status getStatusByStatusId(){
return this.statusByStatusId;
}

public void setStatusByStatusId(Status statusByStatusId){
this.statusByStatusId = statusByStatusId;
}

public Priority getPriorityByPriorityId(){
return this.priorityByPriorityId;
}

public void setPriorityByPriorityId(Priority priorityByPriorityId){
this.priorityByPriorityId = priorityByPriorityId;
}

public Phases getPhasesByPhasesId(){
return this.phasesByPhasesId;
}

public void setPhasesByPhasesId(Phases phasesByPhasesId){
this.phasesByPhasesId = phasesByPhasesId;
}

public UserProject getUserProjectByUserProjectId(){
return this.userProjectByUserProjectId;
}

public void setUserProjectByUserProjectId(UserProject userProjectByUserProjectId){
this.userProjectByUserProjectId = userProjectByUserProjectId;
}

public WeekMaster getWeekMasterByWeekMasterId(){
return this.weekMasterByWeekMasterId;
}

public void setWeekMasterByWeekMasterId(WeekMaster weekMasterByWeekMasterId){
this.weekMasterByWeekMasterId = weekMasterByWeekMasterId;
}
}