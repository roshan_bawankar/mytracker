package com.springmvc.entity;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import org.hibernate.annotations.Parameter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.hibernate.annotations.GenericGenerator;
@EnableTransactionManagement
@Entity
@Table(name="priority")
public class Priority{

@Id
@GeneratedValue
@Column(name="priority_id",nullable=false)
private Integer priorityId;

@Column(name="priority_name",nullable=true,length=20)
private String priorityName;

@OneToMany(fetch=FetchType.LAZY,mappedBy="priorityId")
private Set<WeeklyTimesheet> weeklyTimesheetSetByPriorityId;


public Integer getPriorityId(){
return this.priorityId;
}

public void setPriorityId(Integer priorityId){
this.priorityId = priorityId;
}

public String getPriorityName(){
return this.priorityName;
}

public void setPriorityName(String priorityName){
this.priorityName = priorityName;
}

public Set<WeeklyTimesheet> getWeeklyTimesheetSetByPriorityId(){
return this.weeklyTimesheetSetByPriorityId;
}

public void setWeeklyTimesheetSetByPriorityId(Set<WeeklyTimesheet> weeklyTimesheetSetByPriorityId){
this.weeklyTimesheetSetByPriorityId = weeklyTimesheetSetByPriorityId;
}
}