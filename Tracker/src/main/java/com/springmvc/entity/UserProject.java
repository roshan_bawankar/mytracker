package com.springmvc.entity;
import java.math.BigDecimal;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import org.hibernate.annotations.Parameter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.hibernate.annotations.GenericGenerator;
@EnableTransactionManagement
@Entity
@Table(name="user_project")
public class UserProject{

@Id
@GeneratedValue
@Column(name="user_project_id",nullable=false)
private Integer userProjectId;

@Column(name="user_project_name",nullable=true)
private String userProjectName;

@Column(name="project_id",nullable=true)
private Integer projectId;

@Column(name="aa_type",nullable=true,length=10)
private String aaType;

@Column(name="act_type",nullable=true,length=10)
private String actType;

@Column(name="network",nullable=true,length=10)
private String network;

@Column(name="activity",nullable=true,length=10)
private String activity;

@Column(name="activity_no",nullable=true,length=10)
private String activityNo;

@Column(name="description",nullable=true,length=100)
private String description;

@Column(name="estimated_hours",nullable=true)
private BigDecimal estimatedHours;

@Column(name="actual_hours",nullable=true)
private BigDecimal actualHours;


@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name ="project_id",insertable=false,updatable=false)
private Project projectByProjectId;

@OneToMany(fetch=FetchType.LAZY,mappedBy="userProjectId")
private Set<WeeklyTimesheet> weeklyTimesheetSetByUserProjectId;


public String getUserProjectName() {
	return userProjectName;
}

public void setUserProjectName(String userProjectName) {
	this.userProjectName = userProjectName;
}

public BigDecimal getEstimatedHours() {
	return estimatedHours;
}

public void setEstimatedHours(BigDecimal estimatedHours) {
	this.estimatedHours = estimatedHours;
}

public BigDecimal getActualHours() {
	return actualHours;
}

public void setActualHours(BigDecimal actualHours) {
	this.actualHours = actualHours;
}

public Integer getUserProjectId(){
return this.userProjectId;
}

public void setUserProjectId(Integer userProjectId){
this.userProjectId = userProjectId;
}

public Integer getProjectId(){
return this.projectId;
}

public void setProjectId(Integer projectId){
this.projectId = projectId;
}

public String getAaType(){
return this.aaType;
}

public void setAaType(String aaType){
this.aaType = aaType;
}

public String getActType(){
return this.actType;
}

public void setActType(String actType){
this.actType = actType;
}

public String getNetwork(){
return this.network;
}

public void setNetwork(String network){
this.network = network;
}

public String getActivity(){
return this.activity;
}

public void setActivity(String activity){
this.activity = activity;
}

public String getActivityNo(){
return this.activityNo;
}

public void setActivityNo(String activityNo){
this.activityNo = activityNo;
}

public String getDescription(){
return this.description;
}

public void setDescription(String description){
this.description = description;
}

public Project getProjectByProjectId(){
return this.projectByProjectId;
}

public void setProjectByProjectId(Project projectByProjectId){
this.projectByProjectId = projectByProjectId;
}

public Set<WeeklyTimesheet> getWeeklyTimesheetSetByUserProjectId(){
return this.weeklyTimesheetSetByUserProjectId;
}

public void setWeeklyTimesheetSetByUserProjectId(Set<WeeklyTimesheet> weeklyTimesheetSetByUserProjectId){
this.weeklyTimesheetSetByUserProjectId = weeklyTimesheetSetByUserProjectId;
}
}