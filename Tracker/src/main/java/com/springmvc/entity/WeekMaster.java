package com.springmvc.entity;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="week_master")
public class WeekMaster{

@Id
@GeneratedValue
@Column(name="week_master_id",nullable=false)
private Integer weekMasterId;

@Column(name="week_no",nullable=true)
private Integer weekNo;

@Column(name="from_date",nullable=true)
private Date fromDate;

@Column(name="to_date",nullable=true)
private Date toDate;

@Column(name="username",nullable=true,length=20)
private String username;

@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name ="username",insertable=false,updatable=false)
private User userDetailByUsername;


public Integer getWeekMasterId(){
return this.weekMasterId;
}

public void setWeekMasterId(Integer weekMasterId){
this.weekMasterId = weekMasterId;
}

public Integer getWeekNo(){
return this.weekNo;
}

public void setWeekNo(Integer weekNo){
this.weekNo = weekNo;
}

public Date getFromDate(){
return this.fromDate;
}

public void setFromDate(Date fromDate){
this.fromDate = fromDate;
}

public Date getToDate(){
return this.toDate;
}

public void setToDate(Date toDate){
this.toDate = toDate;
}

public String getUsername(){
return this.username;
}

public void setUsername(String username){
this.username = username;
}

public User getUserDetailByUsername(){
return this.userDetailByUsername;
}

public void setUserDetailByUsername(User userDetailByUsername){
this.userDetailByUsername = userDetailByUsername;
}
}