package com.springmvc.entity;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import org.hibernate.annotations.Parameter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.hibernate.annotations.GenericGenerator;
@EnableTransactionManagement
@Entity
@Table(name="project")
public class Project{

@Id
@GeneratedValue
@Column(name="project_id",nullable=false)
private Integer projectId;

@Column(name="project_name",nullable=true,length=50)
private String projectName;

@OneToMany(fetch=FetchType.LAZY,mappedBy="projectId")
private Set<UserProject> userProjectSetByProjectId;


public Integer getProjectId(){
return this.projectId;
}

public void setProjectId(Integer projectId){
this.projectId = projectId;
}

public String getProjectName(){
return this.projectName;
}

public void setProjectName(String projectName){
this.projectName = projectName;
}

public Set<UserProject> getUserProjectSetByProjectId(){
return this.userProjectSetByProjectId;
}

public void setUserProjectSetByProjectId(Set<UserProject> userProjectSetByProjectId){
this.userProjectSetByProjectId = userProjectSetByProjectId;
}
}