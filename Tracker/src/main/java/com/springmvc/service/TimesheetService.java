/**
 * 
 */
package com.springmvc.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springmvc.dao.DaoImp;
import com.springmvc.entity.Project;
import com.springmvc.entity.User;
import com.springmvc.entity.UserProject;
import com.springmvc.entity.UserProjectToUser;
import com.springmvc.entity.WeekMaster;
import com.springmvc.entity.WeeklyTimesheet;
import com.springmvc.model.ProjectNamesModel;
import com.springmvc.model.UserDetailModel;
import com.springmvc.model.UserProjectNamesModel;
import com.springmvc.model.WeekMasterModel;
import com.springmvc.model.WeeklyTimesheetModel;

/**
 * @author rbawa
 *
 */
@Service(value = "TimesheetService")
public class TimesheetService {

	@Autowired
	@Qualifier("Dao")
	DaoImp dao;

	@Transactional(readOnly = true)
	public List<WeeklyTimesheetModel> populateGrid(WeekMasterModel wmm, UserDetailModel udm) {
		List<WeeklyTimesheetModel> weeklyTimesheetModelList = new ArrayList<WeeklyTimesheetModel>();
		Session session = dao.getCurrentSession();

		try {
			Criteria weeklyTimesheetCr = session.createCriteria(WeeklyTimesheet.class, "wt");
			weeklyTimesheetCr.createAlias("wt.weekMasterByWeekMasterId", "wm");// 3
			weeklyTimesheetCr.add(Restrictions.eq("wm.weekNo", wmm.getWeekNo()));
			weeklyTimesheetCr.add(Restrictions.eq("wm.username", udm.getUsername()));
			List<WeeklyTimesheet> weeklyTimesheetList = (List<WeeklyTimesheet>) dao.getByCriteria(weeklyTimesheetCr);
			if (weeklyTimesheetList.isEmpty()) {
				throw new RuntimeException("weeklyTimesheetList is empty");
			}
			for (WeeklyTimesheet weeklyTimesheet : weeklyTimesheetList) {
				WeeklyTimesheetModel weeklyTimesheetModel = new WeeklyTimesheetModel();
				if (weeklyTimesheet.getUserProjectByUserProjectId() != null) {
					weeklyTimesheetModel.setUserProjectId(weeklyTimesheet.getUserProjectId());
					weeklyTimesheetModel.setUserProjectName(weeklyTimesheet.getUserProjectByUserProjectId().getUserProjectName());
					weeklyTimesheetModel.setActualHours(weeklyTimesheet.getUserProjectByUserProjectId().getActualHours());
					
				}
				if (weeklyTimesheet.getWeekMasterId() != null) {
					weeklyTimesheetModel.setWeekNo(weeklyTimesheet.getWeekMasterByWeekMasterId().getWeekNo());
				}
				
				//Copies properties of weekly timesheet to weekly timesheet modal
				BeanUtils.copyProperties(weeklyTimesheet, weeklyTimesheetModel);
				weeklyTimesheetModel.setWeekNo(wmm.getWeekNo());
				weeklyTimesheetModel.setFromDate(wmm.getFromDate());
				weeklyTimesheetModel.setToDate(wmm.getToDate());
				
				
				weeklyTimesheetModelList.add(weeklyTimesheetModel);
			}

		} catch (RuntimeException e) {
			System.out.println("error is :-( " + e);
			return null;
		}
		return weeklyTimesheetModelList;
	}

	@Transactional(readOnly = true)
	public List<UserProjectNamesModel> populateUserProjectNames(UserDetailModel udm) {
		List<UserProjectNamesModel> userProjectNamesModelList = new ArrayList<UserProjectNamesModel>();
		Session session = dao.getCurrentSession();
		try {
			Criteria criteria = session.createCriteria(UserProjectToUser.class);
			criteria.add(Restrictions.eq("username", udm.getUsername()));
			List userProjectList = criteria.list();

			Iterator<UserProjectToUser> itr = userProjectList.iterator();
			while (itr.hasNext()) {

				UserProjectToUser pro = itr.next();
				UserProjectNamesModel userProjectNamesModel = new UserProjectNamesModel();
				userProjectNamesModel.setUserProjectId(pro.getUserProjectId());
				userProjectNamesModel.setUserProjectName(pro.getUserProjectByUserProjectId().getUserProjectName());
				userProjectNamesModelList.add(userProjectNamesModel);
			}		
					
		} catch (RuntimeException e) {
			System.out.println("Error in getting project Names:( " + e);
			return null;
		}

		return userProjectNamesModelList;
	}
	
	@Transactional
	public Boolean saveSelectedTimesheetService(WeeklyTimesheetModel weeklyTimesheetModel,UserDetailModel udm) {
		Boolean status = false;
		UserProject userProject=null;
		BigDecimal actualHours;
		WeekMaster weekMaster =new WeekMaster();
		WeeklyTimesheet weeklyTimesheet=new WeeklyTimesheet();
		BigDecimal mon = weeklyTimesheetModel.getMonday();
		BigDecimal tue = weeklyTimesheetModel.getTuesday();
		BigDecimal wed = weeklyTimesheetModel.getWednesday();
		BigDecimal thu = weeklyTimesheetModel.getThursday();
		BigDecimal fri = weeklyTimesheetModel.getFriday();
		BigDecimal hoursSpentNew = mon.add(tue.add(wed.add(thu.add(fri))));
		BigDecimal hoursSpentOld;
		BeanUtils.copyProperties(weeklyTimesheetModel,weeklyTimesheet);
		
		Session session = dao.getCurrentSession();

		try {
			
			if(weeklyTimesheet.getUserProjectId()==null || weeklyTimesheet.getUserProjectId().equals(""))
			{
				throw new RuntimeException("No User Project Id found in weekly timesheet model ");
			}
			else{
				Criteria cr = session.createCriteria(UserProject.class);
				cr.add(Restrictions.eq("userProjectId", weeklyTimesheet.getUserProjectId()));
				
				List userProjectList= cr.list();
				Iterator<UserProject> itr= userProjectList.iterator();
				//user project is now having a reference to user project
				while(itr.hasNext()){
					userProject=itr.next();
				}
				actualHours = userProject.getActualHours();
			}
			
			if(weeklyTimesheet.getWeekMasterId()==null || weeklyTimesheet.getWeekMasterId().equals("")){
				//create new week master and save record
				weekMaster.setFromDate(weeklyTimesheetModel.getFromDate());
				weekMaster.setToDate(weeklyTimesheetModel.getToDate());
				weekMaster.setWeekNo(weeklyTimesheetModel.getWeekNo());
				weekMaster.setUsername(udm.getUsername());
				
				session.save(weekMaster);
			}
			Criteria criteria =session.createCriteria(WeekMaster.class);
			criteria.add(Restrictions.eq("fromDate", weeklyTimesheetModel.getFromDate()));
			criteria.add(Restrictions.eq("toDate", weeklyTimesheetModel.getToDate()));
			criteria.add(Restrictions.eq("weekNo", weeklyTimesheetModel.getWeekNo()));
			criteria.add(Restrictions.eq("username", udm.getUsername()));
			
			List weekMasterList = criteria.list();
			Iterator<WeekMaster> weekmasteritr= weekMasterList.iterator();
			if(weekmasteritr.hasNext()){
				weekMaster = weekmasteritr.next();
			}
			weeklyTimesheet.setWeekMasterId(weekMaster.getWeekMasterId());
			if(weeklyTimesheet.getWeeklyTimesheetId()==null || weeklyTimesheet.getWeeklyTimesheetId().equals("")){
				//then update timesheet hours and then save timesheet with week master id
				actualHours = actualHours.add(hoursSpentNew);
				
				session.save(weeklyTimesheet);
				status= true;
				
			}else{	
			//In order to update timesheet first get timesheet with timesheet id compare new and old hours booked then update
				WeeklyTimesheet oldWeeklyTimesheetByWeeklyTimesheetId=null;
				
				Criteria cr=session.createCriteria(WeeklyTimesheet.class);
				cr.add(Restrictions.eq("weeklyTimesheetId", weeklyTimesheet.getWeeklyTimesheetId()));
				List list=cr.list();
				Iterator<WeeklyTimesheet> itr= list.iterator();
				while(itr.hasNext())
				{
					oldWeeklyTimesheetByWeeklyTimesheetId = itr.next();
				}
				hoursSpentOld=(oldWeeklyTimesheetByWeeklyTimesheetId.getMonday().add(oldWeeklyTimesheetByWeeklyTimesheetId.getTuesday().add(oldWeeklyTimesheetByWeeklyTimesheetId.getWednesday().add(oldWeeklyTimesheetByWeeklyTimesheetId.getThursday().add(oldWeeklyTimesheetByWeeklyTimesheetId.getFriday())))));
				BeanUtils.copyProperties(weeklyTimesheet, oldWeeklyTimesheetByWeeklyTimesheetId);
				if(hoursSpentNew.compareTo(hoursSpentOld) > 0){
					actualHours=actualHours.add(hoursSpentNew.subtract(hoursSpentOld));
				}
				else{
					actualHours=actualHours.subtract(hoursSpentOld.subtract(hoursSpentNew));
				}
				
				session.update(oldWeeklyTimesheetByWeeklyTimesheetId);
				status=true;
			}
			if(status==true){
				userProject.setActualHours(actualHours);
				session.update(userProject);
				System.out.println("\n\n\n********Actual hours are:"+actualHours+"**********\n\n\n");
			}
		
		} catch (RuntimeException e) {
			System.out.println("In save selected timesheet service error is :-( " + e);
			return null;
		}

		return status;
	}

	public Boolean deleteRow() {
		Boolean status = false;
		return status;
	}

	
}
