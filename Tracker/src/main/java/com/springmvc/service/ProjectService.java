package com.springmvc.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springmvc.dao.DaoImp;
import com.springmvc.entity.Project;
import com.springmvc.entity.UserProject;
import com.springmvc.entity.UserProjectToUser;
import com.springmvc.entity.WeeklyTimesheet;
import com.springmvc.model.ProjectNamesModel;
import com.springmvc.model.UserDetailModel;
import com.springmvc.model.UserProjectDetailsModel;
import com.springmvc.model.UserProjectNamesModel;
import com.springmvc.model.WeekMasterModel;
import com.springmvc.model.WeeklyTimesheetModel;

@Service(value = "ProjectService")
public class ProjectService {

	@Autowired
	@Qualifier("Dao")
	DaoImp dao;

	@Transactional(readOnly = true)
	public List<UserProjectDetailsModel> populateGridWithProjects(WeekMasterModel wmm, UserDetailModel udm) {
		List<UserProjectDetailsModel> userProjectDetailsModelList = new ArrayList<UserProjectDetailsModel>();
		Session session = dao.getCurrentSession();

		try {
			Criteria criteria = session.createCriteria(UserProject.class);
			List userProjects = criteria.list();

			Iterator<UserProject> itr = userProjects.iterator();
			while (itr.hasNext()) {

				UserProject pro = itr.next();
				UserProjectDetailsModel userProjectDetailsModel = new UserProjectDetailsModel();
				userProjectDetailsModel.setUserProjectId(pro.getUserProjectId());
				userProjectDetailsModel.setUserProjectName(pro.getUserProjectName());
				userProjectDetailsModel.setAaType(pro.getAaType());
				userProjectDetailsModel.setActType(pro.getActType());
				userProjectDetailsModel.setActivity(pro.getActivity());
				userProjectDetailsModel.setActivityNo(pro.getActivityNo());
				userProjectDetailsModel.setDescription(pro.getDescription());
				userProjectDetailsModel.setNetwork(pro.getNetwork());
				userProjectDetailsModel.setProjectId(pro.getProjectId());
				userProjectDetailsModel.setActualHours(pro.getActualHours());
				userProjectDetailsModel.setEstimatedHours(pro.getEstimatedHours());
				userProjectDetailsModelList.add(userProjectDetailsModel);
			}
		} catch (RuntimeException e) {
			System.out.println("Error in getting User Project Details:( " + e);
			return null;
		}
		return userProjectDetailsModelList;
	}
	
	@Transactional
	public Boolean updateProjectTable(UserProjectDetailsModel userProjectDetailsModel, UserDetailModel udm) {
		Boolean status = false;

		UserProject userProject= new UserProject();
		userProject.setUserProjectName(userProjectDetailsModel.getUserProjectName());
		userProject.setAaType(userProjectDetailsModel.getAaType());
		userProject.setActivity(userProjectDetailsModel.getActivity());
		userProject.setActivityNo(userProjectDetailsModel.getActivityNo());
		userProject.setActType(userProjectDetailsModel.getActType());
		userProject.setActualHours(userProjectDetailsModel.getActualHours());
		userProject.setDescription(userProjectDetailsModel.getDescription());
		userProject.setEstimatedHours(userProjectDetailsModel.getEstimatedHours());
		userProject.setNetwork(userProjectDetailsModel.getNetwork());
		userProject.setProjectId(userProjectDetailsModel.getProjectId());
		userProject.setUserProjectId(userProjectDetailsModel.getUserProjectId());
		Session session = dao.getCurrentSession();

		try {
			
			if(userProject.getUserProjectId()!=null )
			{
				session.update(userProject);
				System.out.println("user Project updated ");
			}
			else
			{
				session.save(userProject);
				System.out.println("user Project Saved ");
			}
			
			status=true;
			
		} catch (RuntimeException e) {
			System.out.println("error in saving-updating user project :-( " + e);
			return null;
		}
		return status;
	}
	
	@Transactional
	public Boolean assignUserProjectToUser(UserProjectDetailsModel userProjectDetailsModel, UserDetailModel udm) {
		Boolean status = false;

		UserProjectToUser userProjectToUser= new UserProjectToUser();
		Integer userprojectid=userProjectDetailsModel.getUserProjectId();
		String username=udm.getUsername();
		userProjectToUser.setUserProjectId(userprojectid);
		userProjectToUser.setUsername(username);
		Session session = dao.getCurrentSession();
		
		
		try {
				Criteria criteria = session.createCriteria(UserProjectToUser.class);
				List userProjectToUserList = criteria.list();

				Iterator<UserProjectToUser> itr = userProjectToUserList.iterator();
				while (itr.hasNext()) {
					UserProjectToUser pro = itr.next();
					if(pro.getUserProjectId().equals(userprojectid) && pro.getUsername().equals(username)){
						status=true;
						break;
					}
				}
				if(status==false){
					session.save(userProjectToUser);
					status=true;
				}
				
		} catch (RuntimeException e) {
			System.out.println("error in saving-updating user to user project :-( " + e);
			return null;
		}
		return status;
	}
	
	
	@Transactional(readOnly = true)
	public List<UserProjectNamesModel> populateUserProjectNames() {
		List<UserProjectNamesModel> userProjectNamesModelList = new ArrayList<UserProjectNamesModel>();
		Session session = dao.getCurrentSession();
		try {
			Criteria criteria = session.createCriteria(UserProject.class);		
			List projects = criteria.list();

			Iterator<UserProject> itr = projects.iterator();
			while (itr.hasNext()) {

				UserProject pro = itr.next();
				UserProjectNamesModel userProjectNamesModel = new UserProjectNamesModel();
				userProjectNamesModel.setUserProjectId(pro.getUserProjectId());
				userProjectNamesModel.setUserProjectName(pro.getUserProjectName());
				userProjectNamesModelList.add(userProjectNamesModel);
			}		
					
		} catch (RuntimeException e) {
			System.out.println("Error in getting user project Names:( " + e);
			return null;
		}

		return userProjectNamesModelList;
	}

}
