package com.springmvc.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;

public interface Dao {
	public Session getCurrentSession();
	public Object save(Object obj);
	public Object update(Object obj);
	public Object delete(Object obj);
	public List getByCriteria(Criteria criteria) ;
	public Object getUniqueResultByCriteria(Criteria criteria);

}
