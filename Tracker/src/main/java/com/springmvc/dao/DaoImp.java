package com.springmvc.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

@Repository(value="Dao")
public class DaoImp extends HibernateDaoSupport implements Dao{
	@Autowired
    private final SessionFactory sessionFactory;

	@Autowired
	public DaoImp(SessionFactory sessionFactory)
	{
		this.sessionFactory=sessionFactory;
		setSessionFactory(this.sessionFactory);
	}
	public Session getCurrentSession() {
		Session session=null;
		try {
		    session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
			
		return session;
	}


	public Object save(Object obj) {
		//Session session = sessionFactory.getCurrentSession();
		Object returnObj = null;
		Session session=sessionFactory.getCurrentSession();
		if (session != null && session.isOpen() && obj != null) {
			//Transaction transaction = session.beginTransaction();
			try {
				returnObj = session.save(obj);
				//transaction.commit();
			} catch (Exception e) {
				e.getStackTrace();
				//transaction.rollback();
			}
		}
		return returnObj;
	}

	public Object update(Object obj) {
		//Session session = sessionFactory.getCurrentSession();
		Session session=sessionFactory.getCurrentSession();
		if (session != null && session.isOpen() && obj != null) {
			//Transaction transaction = session.beginTransaction();
			try {
				session.update(obj);
				//transaction.commit();
			} catch (Exception e) {
				e.getStackTrace();
				//transaction.rollback();
			}
		}
		return obj;
	}

	public Object delete(Object obj) {
		//Session session = sessionFactory.getCurrentSession();
		Session session=sessionFactory.getCurrentSession();
		if (session != null && session.isOpen() && obj != null) {
			// transaction = session.beginTransaction();
			try {
				session.delete(obj);
				//transaction.commit();
			} catch (Exception e) {
				e.getStackTrace();
				//transaction.rollback();

			}
		}
		return obj;
	}

	public List getByCriteria(Criteria criteria) {
		//Session session = sessionFactory.getCurrentSession();
		Session session=sessionFactory.getCurrentSession();
		List list = null;
		if (session != null && session.isOpen() && criteria != null) {
			try {
				list = criteria.list();
			} catch (Exception e) {
				e.getStackTrace();
			}
		}
		return list;
	}
	
	public Object getUniqueResultByCriteria(Criteria criteria) {
		//Session session = sessionFactory.getCurrentSession();
		Session session=sessionFactory.getCurrentSession();
		Object obj = null;
		if (session != null && session.isOpen() && criteria != null) {
			try {
				obj = criteria.uniqueResult();
			} catch (Exception e) {
				e.getStackTrace();
			}
		}
		return obj;
	}

}
