package com.springmvc.controller;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.springmvc.model.ProjectNamesModel;
import com.springmvc.model.UserDetailModel;
import com.springmvc.model.UserProjectDetailsModel;
import com.springmvc.model.UserProjectNamesModel;
import com.springmvc.model.WeekMasterModel;
import com.springmvc.model.WeeklyTimesheetModel;
import com.springmvc.service.ProjectService;
import com.springmvc.service.TimesheetService;
import com.springmvc.service.UserProfileService;
import com.springmvc.service.UserService;


@Controller
@RequestMapping("/")
@SessionAttributes("roles")
public class ProjectController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	UserProfileService userProfileService;
	
	@Autowired
	MessageSource messageSource;

	@Autowired
	PersistentTokenBasedRememberMeServices persistentTokenBasedRememberMeServices;
	
	@Autowired
	AuthenticationTrustResolver authenticationTrustResolver;
	
	@Autowired
	@Qualifier("ProjectService")
	ProjectService service;
	
	@RequestMapping(value = "/projects", method = RequestMethod.GET)
	public ModelAndView initProjects(HttpServletRequest request) {
		UserDetailModel userDetail= (UserDetailModel)request.getSession().getAttribute("userDetail");
	 	
		ModelAndView modelAndView = new ModelAndView("projects");
		modelAndView.addObject("userDetail",userDetail);
		WeekMasterModel weekMasterModel = new WeekMasterModel();
		
		// Get calendar set to current date and time
        Calendar c = GregorianCalendar.getInstance();
        // Set the calendar to monday of the current week
        c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        System.out.println("Current week = " + c.get(Calendar.WEEK_OF_YEAR));
      
        weekMasterModel.setWeekNo(c.get(Calendar.WEEK_OF_YEAR));
        java.sql.Date sqlStartDate= new java.sql.Date(c.getTimeInMillis());
        c.add(Calendar.DATE, 6);
        java.sql.Date sqlEndDate= new java.sql.Date(c.getTimeInMillis());
        weekMasterModel.setFromDate(sqlStartDate);
        weekMasterModel.setToDate(sqlEndDate);
        
		modelAndView.addObject("weekMaster",weekMasterModel);
		return modelAndView;
   }
	
	@RequestMapping(value = "/homepage", method = RequestMethod.GET)
 	public ModelAndView homepage(HttpServletRequest request) {
		UserDetailModel userDetail=(UserDetailModel)request.getSession().getAttribute("userDetail");
		ModelAndView modelAndView = new ModelAndView("timesheet");
		modelAndView.addObject("userDetail",userDetail);
		WeekMasterModel weekMasterModel = new WeekMasterModel();
		
		// Get calendar set to current date and time
        Calendar c = GregorianCalendar.getInstance();
        // Set the calendar to monday of the current week
        c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        System.out.println("Current week = " + c.get(Calendar.WEEK_OF_YEAR));
      
        weekMasterModel.setWeekNo(c.get(Calendar.WEEK_OF_YEAR));
        java.sql.Date sqlStartDate= new java.sql.Date(c.getTimeInMillis());
        c.add(Calendar.DATE, 6);
        java.sql.Date sqlEndDate= new java.sql.Date(c.getTimeInMillis());
        weekMasterModel.setFromDate(sqlStartDate);
        weekMasterModel.setToDate(sqlEndDate);
        
		modelAndView.addObject("weekMaster",weekMasterModel);
		return modelAndView;

   }
	
	//To Populate Projects table
	 @RequestMapping(value="/getUserProjects", method = RequestMethod.POST)
	 public @ResponseBody List<UserProjectDetailsModel> getAllUserProjects(@RequestBody WeekMasterModel wmm, HttpServletRequest request) {
		 UserDetailModel udm = (UserDetailModel) request.getSession().getAttribute("userDetail"); 	
		 List<UserProjectDetailsModel> UserProjectDetailsModelList = service.populateGridWithProjects(wmm, udm);
		 return UserProjectDetailsModelList;
	}
	 
	 //To Save or Update New Project Data 
	 @RequestMapping(value="/saveNewProject", method = RequestMethod.POST)
	 public @ResponseBody Boolean updateProjectTable(@RequestBody UserProjectDetailsModel userProjectDetailsModel, HttpServletRequest request) {
		 UserDetailModel udm = (UserDetailModel) request.getSession().getAttribute("userDetail"); 	
		 Boolean status= service.updateProjectTable(userProjectDetailsModel,udm);
		 
		 return status;
	}
	
	// to get user project list in dropdownlist
		 @RequestMapping(value="/getUserProjectList", method = RequestMethod.POST)
		 public @ResponseBody List<UserProjectNamesModel> getProjectDetails( HttpServletRequest request) { 	
			 List<UserProjectNamesModel> userProjectNamesModel = service.populateUserProjectNames();
			 
			 return userProjectNamesModel;
		}
	
		//To Save or Update New Project Data 
		 @RequestMapping(value="/assignProject", method = RequestMethod.POST)
		 public @ResponseBody Boolean assignProject(@RequestBody UserProjectDetailsModel userProjectDetailsModel, HttpServletRequest request) {
			 UserDetailModel udm = (UserDetailModel) request.getSession().getAttribute("userDetail"); 	
			 Boolean status= service.assignUserProjectToUser(userProjectDetailsModel,udm);
			 
			 return status;
		}
		 
		 

}
