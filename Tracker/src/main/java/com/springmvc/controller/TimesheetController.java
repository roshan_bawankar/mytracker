/**
 * 
 */
package com.springmvc.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
//List and ArrayList
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpRequest;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
//Spring Framework
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mchange.net.SocketUtils;
import com.springmvc.dao.DaoImp;
import com.springmvc.entity.Phases;
import com.springmvc.entity.Priority;
import com.springmvc.entity.Project;
import com.springmvc.entity.Status;
import com.springmvc.entity.User;
import com.springmvc.entity.WeekMaster;
import com.springmvc.entity.WeeklyTimesheet;
import com.springmvc.model.ProjectNamesModel;
import com.springmvc.model.UserDetailModel;
import com.springmvc.model.UserProjectDetailsModel;
import com.springmvc.model.UserProjectNamesModel;
import com.springmvc.model.WeekMasterModel;
import com.springmvc.model.WeeklyTimesheetModel;
import com.springmvc.service.TimesheetService;
import com.springmvc.service.UserProfileService;
import com.springmvc.service.UserService;
/**
 * @author rbawa
 *
 */


@Controller
@RequestMapping("/")
@SessionAttributes("roles")
public class TimesheetController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	UserProfileService userProfileService;
	
	@Autowired
	MessageSource messageSource;

	@Autowired
	PersistentTokenBasedRememberMeServices persistentTokenBasedRememberMeServices;
	
	@Autowired
	AuthenticationTrustResolver authenticationTrustResolver;
	
	@Autowired
	@Qualifier("TimesheetService")
	TimesheetService service;
	
		//To Populate table
		 @RequestMapping(value={"/getTimesheet"}, method = RequestMethod.POST)
		 public @ResponseBody List<WeeklyTimesheetModel> getData(@RequestBody WeekMasterModel wmm,HttpServletRequest request) {
			 UserDetailModel udm = (UserDetailModel) request.getSession().getAttribute("userDetail"); 	
			 List<WeeklyTimesheetModel> weeklyTimesheetModelList = service.populateGrid(wmm, udm);
			 return weeklyTimesheetModelList;
		}
		//To Populate ProjectName by User
		 @RequestMapping(value={"/getUserProjectNames"}, method = RequestMethod.POST)
		 public @ResponseBody List<UserProjectNamesModel> getUserProjectNames( HttpServletRequest request) {
			 UserDetailModel udm = (UserDetailModel) request.getSession().getAttribute("userDetail");
			 List<UserProjectNamesModel> userProjectNamesModelList = service.populateUserProjectNames(udm);
			 System.out.println(userProjectNamesModelList.toString());
			 return userProjectNamesModelList;
		}
		 //To Update row Data
		 @RequestMapping(value={"/saveSelectedTimesheet"}, method = RequestMethod.POST)
		 public @ResponseBody Boolean saveSelectedTimesheet(@RequestBody WeeklyTimesheetModel weeklyTimesheetModel, HttpServletRequest request) {
			System.out.println("");
			 UserDetailModel udm = (UserDetailModel) request.getSession().getAttribute("userDetail"); 	
			 Boolean status= service.saveSelectedTimesheetService(weeklyTimesheetModel,udm);
			 
			 return status;
		}
		 
		//To Delete data
		
}


