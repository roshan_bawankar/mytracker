<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
<%@include file="timesheet-head.jsp" %>
<body>

	<%@include file="authheader.jsp" %>

	<section>
		<div>
			<form:form commandName="weekMaster" style="padding:10px">
				<label for="weekno">Week No.:</label>
				<input type="button" value="&#60;" id="previousweek"
					onclick="previousWeek()" />
				<form:input type="text" path="weekNo" id="weekno"
					disabled="disabled" size="2px" />
				<input type="button" value="&#62;" id='nextweek'
					onclick="nextWeek()" />
				<span style="margin-left: 40px"></span>
				<label for="fromdate">From</label>
				<form:input type="date" path="fromDate" id="fromdate"
					readonly="true" size="11px" />
				<label for="todate">To</label>
				<form:input type="date" path="toDate" id="todate" readonly="true"
					size="11px" />
				<input type="button" value="Projects" id="projectsbutton" onclick="projectsbuttonClick()"/>
				<input type="button" value="Export to Excel" id='excelExport'
					style="float: right;" />
			</form:form>
			<div id='jqxWidget' align="center" class="timesheetTable">
				<div id="jqxgrid"></div>
			</div>
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		</div>
		<!-- container -->
	</section>
</body>
</html>