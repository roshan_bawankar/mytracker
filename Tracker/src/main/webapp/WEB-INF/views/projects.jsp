<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE html>
<html>
<%@include file="project-head.jsp" %>
<body>
	<%@include file="authheader.jsp" %>
	<section>
		<div>
			<form:form commandName="weekMaster" style="padding:10px">
				<label for="weekno">Week No.:</label>
				<input type="button" value="&#60;" id="previousweek"
					onclick="previousWeek()" />
				<form:input type="text" path="weekNo" id="weekno"
					disabled="disabled" size="2px" />
				<input type="button" value="&#62;" id='nextweek'
					onclick="nextWeek()" />
				<span style="margin-left: 40px"></span>
				<label for="fromdate">From</label>
				<form:input type="date" path="fromDate" id="fromdate"
					readonly="true" size="11px" />
				<label for="todate">To</label>
				<form:input type="date" path="toDate" id="todate" readonly="true"
					size="11px" />
				<input type="button" value=" Timesheet " id="timesheet" onclick="timesheetbuttonClick()"/>
				<input type="button" value="Export to Excel" id='excelExport'
					style="float: right;" />
			</form:form>
			<script type="text/javascript">
            $(document).ready(function () {
            	var dropdownlistvalue;
                var url = "${pageContext.request.contextPath}/getUserProjectList";
                // prepare the data
                var source =
                {
                    datatype: "json",
                    datafields: [
                        { name: 'userProjectId' },
                        { name: 'userProjectName' }
                    ],
                    url: url,
                    type:'POST',
                    async: true
                };
                var dataAdapter = new $.jqx.dataAdapter(source);
                // Create a jqxDropDownList
                $("#dropdownList").jqxDropDownList({
                    selectedIndex: 0, source: dataAdapter, displayMember: "userProjectName", valueMember: "userProjectId", width: 200, height: 25
                });
                $("#jqxButton").jqxButton({ width: 120, height: 25 });
                // subscribe to the select event.
                $("#dropdownList").on('select', function (event) {
                    if (event.args) {
                        var item = event.args.item;
                        if (item) {
                            var valueelement = $("<div></div>");
                            dropdownlistvalue=item.value;
                            valueelement.text("Value: " + item.value);
                            var labelelement = $("<div></div>");
                            labelelement.text("Label: " + item.label);
                            $("#selectionlog").children().remove();
                            $("#selectionlog").append(labelelement);
                            $("#selectionlog").append(valueelement);
                        }
                    }
                });
             	// Subscribe to Click events.
                $("#jqxButton").on('click', function ()
                {
                	var json = {
                			"userProjectId" : dropdownlistvalue
                		}
                	$.ajax({
            			contentType : 'application/json; charset=UTF-8',
            			url : "${pageContext.request.contextPath}/assignProject",
            			type : "POST",
            			dataType : "json",
            			data : JSON.stringify(json),
            			success : function(dataObj, textStatus, jqXHR) {
            				alert("Assigned Successfully");
            			},
            			error : function(){
						    alert('error!');
						}
            		});
                });
            });
            
        </script>
        	<table>
        		<tr>
        			<td>Select Projects for yourself:</td>
					<td><div id='dropdownList' align="center"></div></td>
					<td><input type="button" value="Assign Project" id='jqxButton' /></td>
					<td><div style="font-size: 12px; font-family: Verdana;" id="selectionlog"></div></td>
				</tr>
			</table>
			<div id='jqxWidget' align="center" class="timesheetTable">
				<div id="jqxgrid"></div>
			</div>
		</div>
			
		<!-- container -->
	</section>
</body>
</html>