<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login page</title>
<link href="<c:url value='/static/css/style.css' />" rel="stylesheet"></link>
</head>

<body>
	<div class="container">
		<section id="content">
			<c:url var="loginUrl" value="/login" />
			<form action="${loginUrl}" method="post">
				<h1>My Tracker</h1>
				<c:if test="${param.error != null}">
					<div class="alert alert-danger">
						<p>Invalid username and password.</p>
					</div>
				</c:if>
				<c:if test="${param.logout != null}">
					<div class="alert alert-success">
						<p>You have been logged out successfully.</p>
					</div>
				</c:if>
				<div>
					<input type="text"  id="username" name="username" placeholder="Enter Username" required />
				</div>
				<div>
					<input type="password"  id="password" name="password" placeholder="Enter Password" required />
				</div>
				<div>
					<label><input type="checkbox" id="rememberme" name="remember-me"/> Remember Me</label>
				</div>
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				<div>
					<input type="submit" value="Log in" /> <a href="#">Lost your
						password?</a> <a href="#">Register</a>
				</div>
			</form>
			<!-- form -->
		</section>
		<!-- content -->
	</div>
	<!-- container -->
</body>
</html>




