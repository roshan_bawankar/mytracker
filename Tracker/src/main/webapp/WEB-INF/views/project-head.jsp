<head>
<title>Projects</title>

<%@include file="resources.jsp" %>

<!--Script For View  -->

<script type="text/javascript">
	function populateGrid() {

		var txtWeekNo = document.getElementById("weekno").value;
		var txtFromDate = document.getElementById("fromdate").value;
		var txtToDate = document.getElementById("todate").value;
		var json = {
			"weekNo" : txtWeekNo, // parameter
			"fromDate" : txtFromDate, // parameter
			"toDate" : txtToDate
		}
		$.ajax({
			contentType : 'application/json; charset=UTF-8',
			url : "${pageContext.request.contextPath}/getUserProjects",
			type : "POST",
			dataType : "json",
			data : JSON.stringify(json),
			success : function(dataObj, textStatus, jqXHR) {

				//Refresh Grid
				data = JSON.stringify(dataObj);
				source.localdata = data;
				// passing "cells" to the 'updatebounddata' method will refresh only the cells values when the new rows count is equal to the previous rows count.
				$("#jqxgrid").jqxGrid('updatebounddata', 'cells');
			}
		});
	}
	
	$(document)
			.ready(
					function() {
						data = [];

						source = {
							datatype : "json",
							datafields : [
							{
								name : 'userProjectId',// 7spentefforts
								type : 'string'
							},              
							{
								name : 'userProjectName',// 7spentefforts
								type : 'string'
							}, {
								name : 'aaType',//8 delDate
								type : 'string'
							}, {
								name : 'actType',// 9dependencies
								type : 'string'
							}, {
								name : 'activity',//10
								type : 'string'
							}, {
								name : 'activityNo',//11
								type : 'string'
							}, {
								name : 'description',//12 total
								type : 'string'
							}, {
								name : 'network',
								type : 'string'
							}, {
								name : 'projectId',
								type : 'string'
							}, {
								name : 'actualHours',
								type : 'string'

							}, {
								name : 'estimatedHours',
								type : 'string'
							} ],
							id:'userProjectId',
							addrow : function(rowid, rowdata, position, commit) {
								// synchronize with the server - send insert command
								// call commit with parameter true if the synchronization with the server is successful 
								//and with parameter false if the synchronization failed.
								// you can pass additional argument to the commit callback which represents the new ID if it is generated from a DB.
								commit(true);
							},
							deleterow : function(rowid, commit) {
								$.ajax({
											url : '${pageContext.request.contextPath}/deleterow',
											data : {
												row : rowid
											},
											type : 'POST',
											success : function(data,
													textStatus, jqXHR) {
												commit(true);
											},
											error : function(jqXHR, textStatus,
													errorThrown) {
												commit(false);
											}
										});
							},
							updaterow : function(rowid, newdata, commit) {
								debugger;
								var oldData = $('#jqxgrid').jqxGrid('getrowdata', rowid);
								
							    commit(true);
								
								
							}
							
						};
						var generaterow = function(i) {
							var row = {};

							/* row["projectId"] = "Please Select"; 
							row["remarks"]   = "Please Select"; //2
							
							
							
							
							row["userProjectId"]	 = "";//7
							row["weekMasterId"] 	= "Enter Data";//8
							
							
							row["weekNo"] 			= "Enter Data";//11
							row["aaType"] 			= "Enter Data";//12
							row["monday"] 			= "Enter Data";//13
							row["tuesday"] 			= "Enter Data";//14
							row["wednesday"] 		= "Enter Data";//15
							row["thusday"] 			= "Enter Data";//16
							row["friday"] 			= "Enter Data";//17
							row["saturday"] 		= "Enter Data";//18
							row["sunday"] 			= "Enter Data";//19  */
 
							return row;
						}
						dataAdapter = new $.jqx.dataAdapter(source);

						// initialize jqxGrid
						$("#jqxgrid")
								.jqxGrid(
										{
											width : $(document).width() - 50,
											source : dataAdapter,
											showtoolbar : true,
											everpresentrowposition : "bottom",
											editable : true,
											enabletooltips : true,
											selectionmode : 'singlerow',
											editmode : 'selectedrow',
											filterable : true,
											sortable : true,
											autoheight : true,
											columnsresize : true,
											pageable : true,
											pagesize : 10,
											pagesizeoptions : [ '1', '2', '3',
													'4', '5', '6', '7', '8',
													'9', '10' ],
											virtualmode : true,
											rendergridrows : function(obj) {
												return obj.data;
											},
											rendertoolbar : function(toolbar) {
												var me = this;
												var container = $("<div style='margin: 5px;'></div>");
												toolbar.append(container);
												container
														.append('<table align="center"><tr><td><input id="addrowbutton" type="button" value="Add New Project" /></td><td><input style="margin-left: 5px;" id="deleterowbutton" type="button" value="Clear Selected" /></td><td><input style="margin-left: 5px;" id="updaterowbutton" type="button" value="Save Selected Project" /></td></tr></table>');
												$("#addrowbutton").jqxButton();
												$("#deleterowbutton")
														.jqxButton();
												$("#updaterowbutton")
														.jqxButton();
												// update row.
												$("#updaterowbutton")
														.on(
																'click',
																function() {
																	var selectedrowindex = $(
																			"#jqxgrid")
																			.jqxGrid(
																					'getselectedrowindex');
																	var rowscount = $(
																			"#jqxgrid")
																			.jqxGrid(
																					'getdatainformation').rowscount;
																	if (selectedrowindex >= 0
																			&& selectedrowindex < rowscount) {
																		var id = $(
																				"#jqxgrid")
																				.jqxGrid(
																						'getrowid',
																						selectedrowindex);
																		debugger;
																		var datarow = $('#jqxgrid')
																				.jqxGrid('getrowdata',selectedrowindex);
																		
																		$.ajax({
																			contentType : 'application/json; charset=UTF-8',
																			url : "${pageContext.request.contextPath}/saveNewProject",
																			type : "POST",
																			dataType : "json",
																			data : JSON.stringify(datarow),
																			success: function(dataObj, textStatus, jqXHR){
																				
																			    alert("Saved Successfully!"+dataObj);
																			  },
																			error: function(){
																			    alert('error!');
																			  }
																		});
																		$("#jqxgrid")
																				.jqxGrid('updaterow',id,datarow);
																		
																	}
																});
												// create new row.

												$("#addrowbutton")
														.on('click',
																function() {
																	var datarow = generaterow();
																	var commit = $(
																			"#jqxgrid")
																			.jqxGrid(
																					'addrow',
																					null,
																					datarow);
																});
												// delete row.
												$("#deleterowbutton")
														.on(
																'click',
																function() {
																	var selectedrowindex = $(
																			"#jqxgrid")
																			.jqxGrid(
																					'getselectedrowindex');
																	var rowscount = $(
																			"#jqxgrid")
																			.jqxGrid(
																					'getdatainformation').rowscount;
																	if (selectedrowindex >= 0
																			&& selectedrowindex < rowscount) {
																		var id = $(
																				"#jqxgrid")
																				.jqxGrid(
																						'getrowid',
																						selectedrowindex);
																		var commit = $(
																				"#jqxgrid")
																				.jqxGrid(
																						'deleterow',
																						id);
																	}
																});
											},
											columns : [
													{
														text : 'User Project Name',
														datafield : 'userProjectName',
														columntype : 'textbox',
														width : 200
													},
													{
														text : 'aa Type',
														datafield : 'aaType',
														columntype : 'textbox',
														width : 200
													},
													{
														text : 'actType',
														datafield : 'actType',
														columntype : 'textbox',
														width : 200,
													},
													{
														text : 'activity',
														datafield : 'activity',
														columntype : 'textbox',
														width : 100
													},
													{
														text : 'activityNo',
														datafield : 'activityNo',
														columntype : 'textbox',
														width : 100
													},
													{
														text : 'description',
														datafield : 'description',
														columntype : 'textbox',
														width : 100
													},
													{
														text : 'network',
														datafield : 'network',
														columntype : 'textbox',
														width : 100
													},
													{
														text : 'projectId',
														datafield : 'projectId',
														columntype : 'textbox',
														width : 100
													},
													{
														text : 'actualHours',
														datafield : 'actualHours',
														columntype : 'textbox',
														width : 100
													},
													{
														text : 'estimatedHours',
														datafield : 'estimatedHours',
														columntype : 'textbox',
														width : 100

													} ]
										});
						$("#excelExport").jqxButton();
						$("#excelExport").click(
								function() {
									$("#jqxgrid").jqxGrid('exportdata', 'xls',
											'jqxGrid');
								});
						populateGrid();
					});
			function timesheetbuttonClick () {
        			location.href = "${pageContext.request.contextPath}/homepage";
    		};
    		
   
</script>
</head>