<head>
<meta name="_csrf" content="${_csrf.token}"/>
	<!-- default header name is X-CSRF-TOKEN -->
<meta name="_csrf_header" content="${_csrf.headerName}"/>
<title>MyTracker</title>

<!-- This file contains all js and css file links -->
<%@include file="resources.jsp" %>


<!--Script For View  -->
<script type="text/javascript">
	var json;
	
	
	
	function populateGrid() {

		var txtWeekNo = document.getElementById("weekno").value;
		var txtFromDate = document.getElementById("fromdate").value;
		var txtToDate = document.getElementById("todate").value;
		json = {
			"weekNo" : txtWeekNo, // parameter
			"fromDate" : txtFromDate, // parameter
			"toDate" : txtToDate
		}
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");
		$.ajax({
			contentType : 'application/json; charset=UTF-8',
			url : "<c:url value='/getTimesheet'/>",
			type : "POST",
			dataType : "json",
			data : JSON.stringify(json),
			beforeSend: function(xhr) {
			        xhr.setRequestHeader(header, token);
			    }, 
			success : function(dataObj) {
				
				//Refresh Grid
				data = JSON.stringify(dataObj);
				source.localdata = data;
				// passing "cells" to the 'updatebounddata' method will refresh only the cells values when the new rows count is equal to the previous rows count.
				$("#jqxgrid").jqxGrid('updatebounddata', 'cells');
			},
			error : function(data) {
		        console.log(data);
		    }
		});
	}
	
	$(document)
			.ready(
					function() {
						data = [];
						
						source = {
							datatype : "json",
							datafields : [ {
								name : 'weekNo',// 7spentefforts
								type : 'number'
							},{
								name : 'actualHours',// 7spentefforts
								type : 'number'
							},{
								name : 'fromDate',// 7spentefforts
								type : 'string'
							},{
								name : 'toDate',// 7spentefforts
								type : 'string'
							},{
								name : 'weeklyTimesheetId',// 7spentefforts
								type : 'number'
							},{
								name : 'userProjectName', //1
								type : 'string'
							}, {
								name : 'userProjectId',// 7spentefforts
								type : 'number'
							}, {
								name : 'weekMasterId',//8 delDate
								type : 'number'
							},{
								name : 'remarks',//10
								type : 'string'
							},{
								name : 'monday',
								type : 'number'
							}, {
								name : 'tuesday',
								type : 'number'
							}, {
								name : 'wednesday',
								type : 'number'
							}, {
								name : 'thursday',
								type : 'number'
							}, {
								name : 'friday',
								type : 'number'
							}, {
								name : 'saturday',
								type : 'number'
							}, {
								name : 'sunday',
								type : 'number'
							} ],
							addrow : function(rowid, rowdata, position, commit) {
								// synchronize with the server - send insert command
								// call commit with parameter true if the synchronization with the server is successful 
								//and with parameter false if the synchronization failed.
								// you can pass additional argument to the commit callback which represents the new ID if it is generated from a DB.
								commit(true);
							},
							deleterow : function(rowid, commit) {
								/* $.ajax({
											url : '${pageContext.request.contextPath}/deleterow',
											data : {
											row : rowid
											},
											type : 'POST',
											success : function(data,
													textStatus, jqXHR) {
												commit(true);
											},
											error : function(jqXHR, textStatus,
													errorThrown) {
												commit(false);
											}
										}); */
								commit(true);
							},
							updaterow : function(rowid, newdata, commit) {
		
								var oldData = $('#jqxgrid').jqxGrid('getrowdata', rowid);
							    commit(true);
								
								
							}
							
						};
						var generaterow = function(i) {
							var row = {};	
							row["weekNo"]=json.weekNo;
							row["fromDate"]=json.fromDate;
							row["toDate"]=json.toDate;
							
							return row;
						}
						dataAdapter = new $.jqx.dataAdapter(source);

						// initialize jqxGrid
						$("#jqxgrid")
								.jqxGrid(
										{
											width : $(document).width() - 50,
											source : dataAdapter,
											showtoolbar : true,
											everpresentrowposition : "bottom",
											editable : true,
											enabletooltips : true,
											selectionmode : 'singlerow',
											editmode : 'selectedrow',
											filterable : true,
											sortable : true,
											autoheight : true,
											columnsresize : true,
											pageable : true,
											pagesize : 10,
											pagesizeoptions : [ '1', '2', '3',
													'4', '5', '6', '7', '8',
													'9', '10' ],
											virtualmode : true,
											rendergridrows : function(obj) {
												return obj.data;
											},
											rendertoolbar : function(toolbar) {
												var me = this;
												var container = $("<div style='margin: 5px;'></div>");
												toolbar.append(container);
												container
														.append('<table align="center"><tr><td><input id="addrowbutton" type="button" value=" Add New " /></td><td><input style="margin-left: 5px;" id="deleterowbutton" type="button" value=" Delete Selected " /></td><td><input style="margin-left: 5px;" id="updaterowbutton" type="button" value=" Save Selected " /></td></tr></table>');
												$("#addrowbutton").jqxButton();
												$("#deleterowbutton")
														.jqxButton();
												$("#updaterowbutton")
														.jqxButton();
												// update row.
												$("#updaterowbutton")
														.on(
																'click',
																function() {
																	var selectedrowindex = $(
																			"#jqxgrid")
																			.jqxGrid(
																					'getselectedrowindex');
																	var rowscount = $(
																			"#jqxgrid")
																			.jqxGrid(
																					'getdatainformation').rowscount;
																	if (selectedrowindex >= 0
																			&& selectedrowindex < rowscount) {
																		var id = $(
																				"#jqxgrid")
																				.jqxGrid(
																						'getrowid',
																						selectedrowindex);
																		debugger;
																		var datarow = $('#jqxgrid').jqxGrid('getrowdata',selectedrowindex);
									
																		$.ajax({
																			contentType : 'application/json; charset=UTF-8',
																			url : "${pageContext.request.contextPath}/saveSelectedTimesheet",
																			type : "POST",
																			dataType : "json",
																			data : JSON.stringify(datarow),
																			success: function(dataObj, textStatus, jqXHR){
																			    alert("Saved Successfully!"+dataObj);
																			  },
																			error: function(){
																			    alert('error!');
																			  }
																		});
															
																		$("#jqxgrid")
																				.jqxGrid('updaterow',id,datarow);
																		
																	}
																});
												// create new row.

												$("#addrowbutton")
														.on('click',
																function() {
																	var datarow = generaterow();
																	var commit = $(
																			"#jqxgrid")
																			.jqxGrid(
																					'addrow',
																					null,
																					datarow);
																});
												// delete row.
												$("#deleterowbutton")
														.on(
																'click',
																function() {
																	var selectedrowindex = $(
																			"#jqxgrid")
																			.jqxGrid(
																					'getselectedrowindex');
																	var rowscount = $(
																			"#jqxgrid")
																			.jqxGrid(
																					'getdatainformation').rowscount;
																	if (selectedrowindex >= 0
																			&& selectedrowindex < rowscount) {
																		var id = $(
																				"#jqxgrid")
																				.jqxGrid(
																						'getrowid',
																						selectedrowindex);
																		var commit = $(
																				"#jqxgrid")
																				.jqxGrid(
																						'deleterow',
																						id);
																	}
																});
											},
											
											columns : [
													{
														text : 'User Project Name',
														datafield : 'userProjectId',
														displayfield: 'userProjectName',
														columntype : 'dropdownlist',
														width : 200,
														createeditor : function(row, column,editor) {
															// assign a new data source to the combobox.
															var list = {
																datatype : "json",
																datafields : [{
																			name : 'userProjectId', type:'number'
																		},{
																			name : 'userProjectName', type:'string'
																		}],
																id: 'userProjectId',
																url : "${pageContext.request.contextPath}/getUserProjectNames",
																async : true,
																type : 'POST'
															};
															var dataAdapter = new $.jqx.dataAdapter(list);
															editor.jqxDropDownList({
																		autoDropDownHeight : true,
																		source : dataAdapter,
																		displayMember : "userProjectName",
																		valueMember : "userProjectId"
																	});
														},
														// update the editor's value before saving it.
														cellvaluechanging : function(row, column,columntype,oldvalue,newvalue) {
															// return the old value, if the new value is empty.
															if (newvalue == "")
																return oldvalue;
														}
													},
													{
														text : 'Remarks',
														datafield : 'remarks',
														columntype : 'textbox',
														width : 400
													},
													{
														text : 'Total',
														datafield : 'actualHours',
														columntype:'numberinput',
														editable:false,
														width : 200,
														validation : function(cell, value) {
															if (value < 0 ) {
																return {
																	result : false,
																	message : ""
																};
															}
															return true;
														}
											

													},
													{
														text : 'Mon',
														datafield : 'monday',
														columntype : 'numberinput',
														width : 100,
														validation : function(cell, value) {
															if (value < 0 || value > 8) {
																return {
																	result : false,
																	message : "Efforts Spent should be greater than zero and less than 9"
																};
															}
															return true;
														},
														createeditor : function(row, cellvalue,editor) {
															editor.jqxNumberInput({
																		decimalDigits : 0
																	});
														}
													},
													{
														text : 'Tue',
														datafield : 'tuesday',
														columntype : 'numberinput',
														width : 100,
														validation : function(cell, value) {
															if (value < 0 || value > 8) {
																return {
																	result : false,
																	message : "Efforts Spent should be greater than zero and less than 9"
																};
															}
															return true;
														},
														createeditor : function(row, cellvalue,editor) {
															editor.jqxNumberInput({
																		decimalDigits : 0
																	});
														}
													},
													{
														text : 'Wed',
														datafield : 'wednesday',
														columntype : 'numberinput',
														width : 100,
														validation : function(cell, value) {
															if (value < 0 || value > 8) {
																return {
																	result : false,
																	message : "Efforts Spent should be greater than zero and less than 9"
																};
															}
															return true;
														},
														createeditor : function(row, cellvalue,editor) {
															editor.jqxNumberInput({
																		decimalDigits : 0
																	});
														}
													},
													{
														text : 'Thu',
														datafield : 'thursday',
														columntype : 'numberinput',
														width : 100,validation : function(cell, value) {
															if (value < 0 || value > 8) {
																return {
																	result : false,
																	message : "Efforts Spent should be greater than zero and less than 9"
																};
															}
															return true;
														},
														createeditor : function(row, cellvalue,editor) {
															editor.jqxNumberInput({
																		decimalDigits : 0
																	});
														}
													},
													{
														text : 'Fri',
														datafield : 'friday',
														columntype : 'numberinput',
														width : 100,
														validation : function(cell, value) {
															if (value < 0 || value > 8) {
																return {
																	result : false,
																	message : "Efforts Spent should be greater than zero and less than 9"
																};
															}
															return true;
														},
														createeditor : function(row, cellvalue,editor) {
															editor.jqxNumberInput({
																		decimalDigits : 0
																	});
														}
													},
													{
														text : 'Sat',
														datafield : 'saturday',
														columntype : 'numberinput',
														editable:false,
														width : 100,
														validation : function(cell, value) {
															if (value < 0 || value > 8) {
																return {
																	result : false,
																	message : "Efforts Spent should be greater than zero and less than 9"
																};
															}
															return true;
														},
														createeditor : function(row, cellvalue,editor) {
															editor.jqxNumberInput({
																		decimalDigits : 0
																	});
														}
													},
													{
														text : 'Sun',
														datafield : 'sunday',
														columntype : 'numberinput',
														editable:false,
														width : 100,
														validation : function(cell, value) {
															if (value < 0 || value > 8) {
																return {
																	result : false,
																	message : "Efforts Spent should be greater than zero and less than 9"
																};
															}
															return true;
														},
														createeditor : function(row, cellvalue,editor) {
															editor.jqxNumberInput({
																		decimalDigits : 0
																	});
														}

													} ]
										});
						$("#excelExport").jqxButton();
						$("#excelExport").click(
								function() {
									$("#jqxgrid").jqxGrid('exportdata', 'xls',
											'jqxGrid');
								});
						populateGrid();
					});
			function projectsbuttonClick () {
        			location.href = "${pageContext.request.contextPath}/projects";
    		};
</script>
</head>