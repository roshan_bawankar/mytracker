var source;
	var data;
	var dataAdapter;
	// Returns the ISO week of the date.
	Date.prototype.getWeek = function() {
		var date = new Date(this.getTime());
		date.setHours(0, 0, 0, 0);
		// Thursday in current week decides the year.
		date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
		// January 4 is always in week 1.
		var week1 = new Date(date.getFullYear(), 0, 4);
		// Adjust to Thursday in week 1 and count number of weeks from date to
		// week1.
		return 1 + Math
				.round(((date.getTime() - week1.getTime()) / 86400000 - 3 + (week1
						.getDay() + 6) % 7) / 7);
	}

	function setWeekEndDate(year, month, day, no_of_days) {
		var last_day_of_month = 0;
		if (month == 2) {
			if (((year % 100 != 0) && (year % 4 == 0)) || (year % 400 == 0)) {
				last_day_of_month = 29;
			} else {
				last_day_of_month = 28;
			}
		} else {
			if (month == 1 || month == 3 || month == 5 || month == 7
					|| month == 8 || month == 10 || month == 12) {
				last_day_of_month = 31;
			}
			if (month == 4 || month == 6 || month == 9 || month == 11) {
				last_day_of_month = 30;
			}
		}
		var increased_date = day + no_of_days;
		if (increased_date > last_day_of_month) {
			increased_date = increased_date - last_day_of_month;
			month = month + 1;
			if (month > 12) {
				month = 1;
				year = year + 1;
			}
		}

		var endDate = String(increased_date)
		var endmonth = String(month);
		var endyear = String(year);

		if (endmonth.length < 2)
			endmonth = '0' + endmonth;
		if (endDate.length < 2)
			endDate = '0' + endDate;

		document.getElementById("todate").value = endyear + "-" + endmonth
				+ "-" + endDate;
	}

	function setDateOfISOWeek(fromdate) {
		var simple = fromdate;
		var dow = simple.getDay();
		var ISOweekStart = simple;
		if (dow <= 4)
			ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
		else
			ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());

		let
		month = String(ISOweekStart.getMonth() + 1);
		let
		day = String(ISOweekStart.getDate());
		const
		year = String(ISOweekStart.getFullYear());
		if (month.length < 2)
			month = '0' + month;
		if (day.length < 2)
			day = '0' + day;
		document.getElementById("weekno").value = simple.getWeek();

		document.getElementById("fromdate").value = year + "-" + month + "-"
				+ day;

		setWeekEndDate(ISOweekStart.getFullYear(),
				(ISOweekStart.getMonth() + 1), ISOweekStart.getDate(), 6);

		return simple.getWeek();

	}

	
	
	function previousWeek() {
		
		var weekNo = parseInt(document.getElementById("weekno").value);
		var fromdateObj = document.getElementById("fromdate");
		var fromdate = new Date(fromdateObj.valueAsDate);
		fromdate.setTime(fromdate.getTime() - 604800000);
		weekNo = setDateOfISOWeek(fromdate);

		populateGrid();
	}

	function nextWeek() {
		var weekNo = parseInt(document.getElementById("weekno").value);
		var fromdateObj = document.getElementById("fromdate");
		var fromdate = new Date(fromdateObj.valueAsDate);
		fromdate.setTime(fromdate.getTime() + 604800000);
		weekNo = setDateOfISOWeek(fromdate);

		populateGrid();
	}
	
		
