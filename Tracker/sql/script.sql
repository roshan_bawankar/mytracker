/*
 * Execute this script before before run the app 
 * Database: MySQL
 * 
 * Author: Roshan Bawankar
 * 
 
/*All User's gets stored in APP_USER table*/
create table APP_USER (
   id BIGINT NOT NULL AUTO_INCREMENT,
   sso_id VARCHAR(30) NOT NULL,
   password VARCHAR(100) NOT NULL,
   first_name VARCHAR(30) NOT NULL,
   last_name  VARCHAR(30) NOT NULL,
   email VARCHAR(30) NOT NULL,
   PRIMARY KEY (id),
   UNIQUE (sso_id)
);
   
/* USER_PROFILE table contains all possible roles */ 
create table USER_PROFILE(
   id BIGINT NOT NULL AUTO_INCREMENT,
   type VARCHAR(30) NOT NULL,
   PRIMARY KEY (id),
   UNIQUE (type)
);
   
/* JOIN TABLE for MANY-TO-MANY relationship*/  
CREATE TABLE APP_USER_USER_PROFILE (
    user_id BIGINT NOT NULL,
    user_profile_id BIGINT NOT NULL,
    PRIMARY KEY (user_id, user_profile_id),
    CONSTRAINT FK_APP_USER FOREIGN KEY (user_id) REFERENCES APP_USER (id),
    CONSTRAINT FK_USER_PROFILE FOREIGN KEY (user_profile_id) REFERENCES USER_PROFILE (id)
);
  
/* Populate USER_PROFILE Table */
INSERT INTO USER_PROFILE(type)
VALUES ('USER');
  
INSERT INTO USER_PROFILE(type)
VALUES ('ADMIN');
  
INSERT INTO USER_PROFILE(type)
VALUES ('DBA');
  
  
/* Populate one Admin User which will further create other users for the application using GUI */
INSERT INTO APP_USER(sso_id, password, first_name, last_name, email)
VALUES ('rbawa','$2a$10$4eqIF5s/ewJwHK1p8lqlFOEm2QIA0S8g6./Lok.pQxqcxaBZYChRm', 'Roshan','Bawankar','rbawa@nets.eu');
  
  
/* Populate JOIN Table */
INSERT INTO APP_USER_USER_PROFILE (user_id, user_profile_id)
  SELECT user.id, profile.id FROM app_user user, user_profile profile
  where user.sso_id='rbawa' and profile.type='ADMIN';
 
/* Create persistent_logins Table used to store rememberme related stuff*/
CREATE TABLE persistent_logins (
    username VARCHAR(64) NOT NULL,
    series VARCHAR(64) NOT NULL,
    token VARCHAR(64) NOT NULL,
    last_used TIMESTAMP NOT NULL,
    PRIMARY KEY (series)
);









DROP TABLE IF EXISTS `my_tracker`.`phases`;
CREATE TABLE  `my_tracker`.`phases` (
  `phase_id` int(11) NOT NULL AUTO_INCREMENT,
  `phase_name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`phase_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `my_tracker`.`priority`;
CREATE TABLE  `my_tracker`.`priority` (
  `priority_id` int(11) NOT NULL AUTO_INCREMENT,
  `priority_name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`priority_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `my_tracker`.`project`;
CREATE TABLE  `my_tracker`.`project` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `my_tracker`.`role`;
CREATE TABLE  `my_tracker`.`role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(25) DEFAULT NULL,
  `role_code` varchar(20) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `my_tracker`.`status`;
CREATE TABLE  `my_tracker`.`status` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `my_tracker`.`user_detail`;
CREATE TABLE  `my_tracker`.`user_detail` (
  `username` varchar(20) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `personnel_no` varchar(10) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `password` varchar(30) NOT NULL,
  `role_code` varchar(20) NOT NULL,
  PRIMARY KEY (`username`),
  KEY `FK_13wnlsx5glnwgwayst479p1lk` (`role_id`),
  CONSTRAINT `FK_13wnlsx5glnwgwayst479p1lk` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `my_tracker`.`user_project`;
CREATE TABLE  `my_tracker`.`user_project` (
  `user_project_id` int(11) NOT NULL AUTO_INCREMENT,
  `aa_type` varchar(10) DEFAULT NULL,
  `act_type` varchar(10) DEFAULT NULL,
  `activity` varchar(10) DEFAULT NULL,
  `activity_no` varchar(10) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `network` varchar(10) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_project_id`),
  KEY `FK_18y5rtfs91x51elpfsbkx35rj` (`project_id`),
  KEY `FK_2pb7vyyvcyc494f2u24xo5ehb` (`username`),
  CONSTRAINT `FK_18y5rtfs91x51elpfsbkx35rj` FOREIGN KEY (`project_id`) REFERENCES `project` (`project_id`),
  CONSTRAINT `FK_2pb7vyyvcyc494f2u24xo5ehb` FOREIGN KEY (`username`) REFERENCES `user_detail` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `my_tracker`.`week_master`;
CREATE TABLE  `my_tracker`.`week_master` (
  `week_master_id` int(11) NOT NULL AUTO_INCREMENT,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `week_no` int(11) DEFAULT NULL,
  PRIMARY KEY (`week_master_id`),
  KEY `FK_7kol5b0fq6o7upsl3p3ni290b` (`username`),
  CONSTRAINT `FK_7kol5b0fq6o7upsl3p3ni290b` FOREIGN KEY (`username`) REFERENCES `user_detail` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `my_tracker`.`weekly_timesheet`;
CREATE TABLE  `my_tracker`.`weekly_timesheet` (
  `weekly_timesheet_id` int(11) NOT NULL AUTO_INCREMENT,
  `friday` decimal(4,2) DEFAULT NULL,
  `monday` decimal(4,2) DEFAULT NULL,
  `phases_id` int(11) DEFAULT NULL,
  `priority_id` int(11) DEFAULT NULL,
  `remarks` varchar(100) DEFAULT NULL,
  `saturday` decimal(4,2) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `sunday` decimal(4,2) DEFAULT NULL,
  `thursday` decimal(4,2) DEFAULT NULL,
  `tuesday` decimal(4,2) DEFAULT NULL,
  `user_project_id` int(11) DEFAULT NULL,
  `wednesday` decimal(4,2) DEFAULT NULL,
  `week_master_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`weekly_timesheet_id`),
  KEY `FK_9khmq67xr93i1jpbe1scjrugk` (`phases_id`),
  KEY `FK_78nmtyp8grrn71cuqhwhva0jr` (`priority_id`),
  KEY `FK_mabr0f6a6rr1hxiyxmwcmi3db` (`status_id`),
  KEY `FK_ka5qsfk2785ag15lg8l6vlota` (`user_project_id`),
  KEY `FK_ou97xvyosa258wtl1xe7hvo9r` (`week_master_id`),
  CONSTRAINT `FK_78nmtyp8grrn71cuqhwhva0jr` FOREIGN KEY (`priority_id`) REFERENCES `priority` (`priority_id`),
  CONSTRAINT `FK_9khmq67xr93i1jpbe1scjrugk` FOREIGN KEY (`phases_id`) REFERENCES `phases` (`phase_id`),
  CONSTRAINT `FK_ka5qsfk2785ag15lg8l6vlota` FOREIGN KEY (`user_project_id`) REFERENCES `user_project` (`user_project_id`),
  CONSTRAINT `FK_mabr0f6a6rr1hxiyxmwcmi3db` FOREIGN KEY (`status_id`) REFERENCES `status` (`status_id`),
  CONSTRAINT `FK_ou97xvyosa258wtl1xe7hvo9r` FOREIGN KEY (`week_master_id`) REFERENCES `week_master` (`week_master_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

*/